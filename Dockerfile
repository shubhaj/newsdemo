FROM golang:alpine

# Enable module management
ENV GO111MODULE=on
ENV GOFLAGS=-mod=vendor

# Create matching user in container
RUN mkdir -p /home/newsdemo

COPY .  /home/newsdemo

WORKDIR /home/newsdemo

RUN GOOS=linux GOARCH=amd64 go build main.go

EXPOSE 3000

CMD ["sh", "start.sh"]
