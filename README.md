# News Demo Application

## Prerequisites

- You need to have Go installed on your computer. 
- Sign up for a [News API account](https://newsapi.org/register) and get your free API key.
- API key for this demo is f8fca8f471f64cfea106625944d53eb7

## Get started

- Clone this repository to your filesystem.

- `cd` into it and run the following command: `go run main.go -apikey=<your news api key>` to start the server on port 3000.

- View http://<HOST_IP_OR_LOCALHOST>:3000 in your browser.
